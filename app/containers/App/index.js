/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { withStyles, withTheme } from 'material-ui/styles';
import { Switch, Route } from 'react-router-dom';
// import { FormattedMessage } from 'react-intl';;
// import HomePage from 'containers/HomePage/Loadable';;
import Resume from 'containers/Resume';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { compose } from 'redux';
import Header from 'components/Header';
import Footer from 'components/Footer';
import ReactGA from 'react-ga';
// import messages from '../../containers/Resume/messages';

const AppWrapper = styled.div`
  // max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  // padding: 0 16px;
  flex-direction: column;
`;

const styles = (theme) => ({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
});

class App extends React.Component {

  componentDidMount() {
    ReactGA.initialize('UA-111720492-1');
    ReactGA.pageview(window.location.pathname + window.location.search);
    // console.log('heyheyhey------------->>>>>>>>>>')
    // console.log(window.location.pathname)
    // console.log(window.location.search)
    // console.log(window.location.pathname + window.location.search)
  }

  // noinspection JSMethodCanBeStatic
  render() {
    return (
      <AppWrapper>
        <Helmet
          // titleTemplate="%s - Pharrell.zx WANG"
          titleTemplate="%s"
          defaultTitle="Pharrell.zx WANG"
        >
          <meta name="description" content="Pharrell.zx WANG" />
        </Helmet>
        <Header />
        <Switch>
          <Route exact path="/" component={Resume} />
          {/* <Route exact path="/" component={HomePage}  /> */}
          <Route path="/features" component={FeaturePage} />
          <Route path="" component={NotFoundPage} />
        </Switch>
        <Footer />
      </AppWrapper>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(styles),
  withTheme(),
)(App);
