/**
 *
 * Resume
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import { Helmet } from 'react-helmet';
// import { Link } from 'react-router-dom';
// import { FormattedMessage } from 'react-intl';
import { injectIntl } from 'react-intl';
import { compose } from 'redux';
import classNames from 'classnames';
import List, {
  ListItem,
  ListItemIcon,
  // ListItemSecondaryAction,
  ListItemText,
  // ListSubheader,
} from 'material-ui/List';
// import Switch from 'material-ui/Switch';
// import WifiIcon from 'material-ui-icons/Wifi';
// import ListSubheader from 'material-ui/List/ListSubheader';
// import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
// import Collapse from 'material-ui/transitions/Collapse';
// import InboxIcon from 'material-ui-icons/MoveToInbox';
// import DraftsIcon from 'material-ui-icons/Drafts';
// import SendIcon from 'material-ui-icons/Send';
// import ExpandLess from 'material-ui-icons/ExpandLess';
// import BluetoothIcon from 'material-ui-icons/Bluetooth';
// import ExpandMore from 'material-ui-icons/ExpandMore';
// import StarBorder from 'material-ui-icons/StarBorder';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
import MailOutline from 'material-ui-icons/MailOutline';
// import LocationOn from 'material-ui-icons/LocationOn';
import AccessTime from 'material-ui-icons/AccessTime';
import { HashLink } from 'react-router-hash-link';
// import LinkIcon from 'material-ui-icons/Link';
import Phone from 'material-ui-icons/Phone';
// import messages from './messages';
import { GithubCircle, OpenInNew } from 'mdi-material-ui';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
// import ChatBubble from 'react-chat-bubble';
// import { Emoji } from 'emoji-mart';
// import { Picker } from 'emoji-mart';
// import emoji from 'emoji-dictionary';
// import { emojify } from 'react-emojione';
import AcademiaMScThesisEx from './academia/thesis';
import WorkZwapEx from './work/zwap';
import WorkHkcmEx from './work/hkcm';
import WorkZwapPayEx from './work/zwapPay';
import { styles } from './styles';
// import workFintech from './workFintech';
import pha from './avatars/pharrell.jpg';
import pharrellInOffice from './avatars/pharrell_in_office.jpg';
// import messages from '../../components/Header/messages';
import messages from './messages';
// import { ColorAWithUnderline } from '../../components/A';

// const styles = styles;]
// const options = {
//   style: {
//     height: 10,
//     margin: 10,
//   },
// };

export class Resume extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    open: true,
  }
  //
  handleClick = () => {
    this.setState({ open: false });
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  }

  render() {
    const { classes } = this.props;
    const bull = <span className={classes.bullet}>•</span>;
    // eslint-disable-next-line react/prop-types
    const { formatMessage } = this.props.intl;
    // const blackBull = <span className={classes.blackBullet}>•</span>;
    // const bs = <span className={classes.blankSpace}>{}</span>;
    return (
      <div>
        <Helmet>
          <title>
            {formatMessage(messages.resumeTitle)}
          </title>
          <meta name="description" content="Description of Resume" />
        </Helmet>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={this.state.open}
          autoHideDuration={10000}
          onClose={this.handleClose}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">
            {formatMessage(messages.lastUpdated)}
          </span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
        <div className={classes.root}>
          <Grid container spacing={8}>
            <Grid item xs={12} sm={12} md={4}>
              <Paper className={classes.paper}>
                <div className={classes.row}>
                  <Avatar
                    alt="Pharrell"
                    src={pha}
                    className={classNames(classes.avatar, classes.bigAvatar)}
                  />
                </div>
                <div className={classes.row}>
                  <Typography type="headline" gutterBottom>
                    {formatMessage(messages.pharrellName)}
                  </Typography>
                </div>
                <div className={classes.row}>
                  <Typography type="caption" gutterBottom>
                    {formatMessage(messages.degreeOnHold)}
                  </Typography>
                </div>
                <Divider light className={classes.dividerMarginBottom} />
                <List>
                  <ListItem button component="a" href="mailto:wzxnuaa@gmail.com">
                    <ListItemIcon>
                      <MailOutline />
                    </ListItemIcon>
                    <ListItemText primary="wzxnuaa@gmail.com" secondary={formatMessage(messages.email)} />
                  </ListItem>
                  <ListItem button component="a" href="https://github.com/PharrellWANG" target="_blank">
                    <ListItemIcon>
                      <GithubCircle />
                    </ListItemIcon>
                    <ListItemText primary={formatMessage(messages.myGitHub)} />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <Phone />
                    </ListItemIcon>
                    <ListItemText inset primary="+852 6524 9387" secondary={formatMessage(messages.HongKong)} />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <Phone />
                    </ListItemIcon>
                    <ListItemText inset primary="+86 1314 380 1247" secondary={formatMessage(messages.Mainland)} />
                  </ListItem>
                </List>
                <Divider light className={classes.dividerMarginBottom} />
                <List>
                  <ListItem button component="a" href="https://git.io/wzx" target="_blank">
                    <ListItemIcon>
                      <OpenInNew />
                    </ListItemIcon>
                    <ListItemText primary="https://git.io/wzx" secondary={formatMessage(messages.resumeTitle)} />
                  </ListItem>
                </List>
                <Divider light className={classes.dividerMarginBottom} />
                <div className={classes.avatarRow}>
                  <Avatar
                    alt="Pharrell.zx WANG"
                    src={pharrellInOffice}
                    className={classNames(classes.avatar2, classes.bigAvatar2)}
                  />
                  <Typography type="body2" style={{ marginTop: 8, marginLeft: 2, marginRight: 2 }}>
                    &gt;_
                  </Typography>
                  <Typography type="body1" component="p" style={{ marginTop: 10, marginLeft: 2 }}>
                    {formatMessage(messages.msgFromAuthor)}&nbsp;🙂
                  </Typography>
                </div>
                {/* <div className={classes.rowPaddingTop}> */}
                {/* {emojify(':D', options)} */}
                {/* </div> */}
                {/* <ChatBubble messages="sdfasd" /> */}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={12} md={8}>
              <Paper className={classes.paper}>
                <div className={classes.leftAlignRow}>
                  <Typography type="display1" gutterBottom>
                    {formatMessage(messages.toc)}
                  </Typography>
                </div>
                <Divider light className={classes.dividerMarginBottom} />
                <div className={classes.tableOfContents}>
                  <div className={classes.leftAlignRow}>
                    <Typography type="body1" gutterBottom>
                      <HashLink className={classes.hashLink} to="#interest">{formatMessage(messages.interests)}</HashLink>
                    </Typography>
                  </div>
                  <div className={classes.leftAlignRow}>
                    <Typography type="body1" gutterBottom>
                      <HashLink className={classes.hashLink} to="#research">{formatMessage(messages.academiaExperience)}</HashLink>
                    </Typography>
                  </div>
                  <div className={classes.leftAlignRow}>
                    <Typography type="body1" gutterBottom>
                      <HashLink className={classes.hashLink} to="#work">{formatMessage(messages.workExperience)}</HashLink>
                    </Typography>
                  </div>
                  <div className={classes.leftAlignRow}>
                    <Typography type="body1" gutterBottom>
                      <HashLink className={classes.hashLink} to="#education">{formatMessage(messages.education)}</HashLink>
                    </Typography>
                  </div>
                </div>
              </Paper>
              <Paper className={classes.paper}>
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                {/* interest */}
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                <div className={classes.leftAlignRowPaddingTop} id="interest">
                  <Typography type="title" gutterBottom>
                    {formatMessage(messages.interests)}
                  </Typography>
                </div>
                <Divider light className={classes.dividerMarginBottom} />
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {formatMessage(messages.DeepLearning)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {formatMessage(messages.VideoCodingOptimizations)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {formatMessage(messages.DataAnalyticsVisu)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {formatMessage(messages.fullStackWebAppDev)}
                  </Typography>
                </div>
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                {/* academia */}
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                <div className={classes.leftAlignRowPaddingTop} id="research">
                  <Typography type="title" gutterBottom>
                    {formatMessage(messages.academiaExperience)}
                  </Typography>
                </div>
                <Divider light className={classes.dividerMarginBottom} />
                <AcademiaMScThesisEx />
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                {/* work */}
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                <div className={classes.leftAlignRowPaddingTop} id="work">
                  <Typography type="title" gutterBottom>
                    {formatMessage(messages.workExperience)}
                  </Typography>
                </div>
                <Divider light className={classes.dividerMarginBottom} />
                <WorkHkcmEx />
                {/* -- */}
                <WorkZwapEx />
                {/* -- */}
                <WorkZwapPayEx />
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                {/* Education */}
                {/* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */}
                <div className={classes.leftAlignRowPaddingTop} id="education">
                  <Typography type="title" gutterBottom>
                    {formatMessage(messages.education)}
                  </Typography>
                </div>
                <Divider light className={classes.dividerMarginBottom} />
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    <strong>
                      {formatMessage(messages.MScUniv)}
                    </strong>
                  </Typography>
                </div>
                <Typography type="caption" gutterBottom>
                  <AccessTime />
                  &nbsp;
                  {formatMessage(messages.MScPeriod)}
                </Typography>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {bull}{formatMessage(messages.Award)}&nbsp;:
                    &nbsp;
                    {formatMessage(messages.MScAward)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {bull}{formatMessage(messages.Major)}&nbsp;:
                    &nbsp;
                    {formatMessage(messages.MScMajor)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {bull}{formatMessage(messages.MScThesis)}&nbsp;:
                    &nbsp;
                    {formatMessage(messages.MScThesisTitle)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    <strong>{formatMessage(messages.BScUniv)}</strong>
                  </Typography>
                </div>
                <Typography type="caption" gutterBottom>
                  <AccessTime />
                  &nbsp;
                  {formatMessage(messages.BScPeriod)}
                </Typography>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {bull}{formatMessage(messages.Award)}&nbsp;:
                    &nbsp;
                    {formatMessage(messages.BScAward)}
                  </Typography>
                </div>
                <div className={classes.leftAlignRow}>
                  <Typography type="body1" gutterBottom>
                    {bull}{formatMessage(messages.Major)}&nbsp;:
                    &nbsp;
                    {formatMessage(messages.BScMajor)}
                  </Typography>
                </div>
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

Resume.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default compose(
  injectIntl,
  withStyles(styles),
)(Resume);
