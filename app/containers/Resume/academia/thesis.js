/**
 *
 * MScThesis
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { compose } from 'redux';
import Typography from 'material-ui/Typography';
import LocationOn from 'material-ui-icons/LocationOn';
import { injectIntl } from 'react-intl';
import AccessTime from 'material-ui-icons/AccessTime';
import { ColorAWithUnderline } from '../../../components/A/index';
import { styles } from '../styles';
import messages from '../messages';


export class MScThesis extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { classes } = this.props;
    const bull = <span className={classes.bullet}>•</span>;
    // eslint-disable-next-line react/prop-types
    const { formatMessage } = this.props.intl;
    return (
      <div>
        {/* xxxxxxxx */}
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            <strong>{formatMessage(messages.MScThesisTitle)}</strong>
          </Typography>
        </div>
        <Typography type="caption" gutterBottom>
          <AccessTime />
          &nbsp;
          {formatMessage(messages.MScThesisPeriod)}
        </Typography>
        <Typography type="caption" gutterBottom>
          <LocationOn />
          &nbsp;
          {formatMessage(messages.MScUniv)}
        </Typography>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.MScGroup)}&nbsp;:
            &nbsp;
            <ColorAWithUnderline href="http://cmsp.eie.polyu.edu.hk/index.htm" target="_blank">
              {formatMessage(messages.MScGroupName)}
            </ColorAWithUnderline>
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.MScSupervisor)}&nbsp;:
            &nbsp;
            <ColorAWithUnderline href="http://www.eie.polyu.edu.hk/~ylchan/" target="_blank">
              {formatMessage(messages.MScSupervisorName)}
            </ColorAWithUnderline>
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.MScThesis)}&nbsp;:
            &nbsp;
            <ColorAWithUnderline href="https://git.io/pharrell-master-thesis" target="_blank">
              Fast Depth Coding in 3D-HEVC Using Deep Learning&nbsp;[&nbsp;PDF&nbsp;]
            </ColorAWithUnderline>
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}LaTeX&nbsp;:
            &nbsp;
            <ColorAWithUnderline href="https://github.com/PharrellWANG/pharrell-master-thesis" target="_blank">
              {formatMessage(messages.LatexMScThesis)}
            </ColorAWithUnderline>
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.MScThesisDocument)}&nbsp;:
            &nbsp;
            <ColorAWithUnderline href="http://fast-depth-coding.readthedocs.io/en/latest/index.html" target="_blank">
              {formatMessage(messages.MScThesisDocumentTitle)}
            </ColorAWithUnderline>
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.MScThesisCodebases)}&nbsp;:
            &nbsp;
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            <ColorAWithUnderline
              href="https://github.com/PharrellWANG/modified_HTM16.2_for_data_collection"
              target="_blank"
            >
              {formatMessage(messages.MScThesisProj1)}&nbsp;
            </ColorAWithUnderline>
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography type="caption" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(C++)
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            <ColorAWithUnderline href="https://github.com/PharrellWANG/data-processing-for-fdc" target="_blank">
              {formatMessage(messages.MScThesisProj2)} &nbsp;
            </ColorAWithUnderline>
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography type="caption" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Python)
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            <ColorAWithUnderline href="https://github.com/PharrellWANG/fdc_resnet_v3" target="_blank">
              {formatMessage(messages.MScThesisProj3)} &nbsp;
            </ColorAWithUnderline>
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography type="caption" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Python)
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            <ColorAWithUnderline href="https://github.com/PharrellWANG/HTM162-Bazel-Cmake" target="_blank">
              {formatMessage(messages.MScThesisProj4)} &nbsp;
            </ColorAWithUnderline>
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography type="caption" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(C++)
            <br />
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.MScThesisContributions)} &nbsp;:
            &nbsp;
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            {formatMessage(messages.MScThesisContribution1)}
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            {formatMessage(messages.MScThesisContribution2)}
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            {formatMessage(messages.MScThesisContribution3)}
            <br />
          </Typography>
        </div>
      </div>
    );
  }
}

MScThesis.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default compose(
  injectIntl,
  withStyles(styles),
)(MScThesis);
