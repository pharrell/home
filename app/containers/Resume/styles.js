/**
 *
 * style
 *
 */

export const styles = (theme) => ({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
  avatarRow: {
    display: 'flex',
    // margin: 4,
    justifyContent: 'left',
    verticalAlign: 'center',
  },
  indentRow: {
    display: 'flex',
    // margin: 4,
    justifyContent: 'left',
    verticalAlign: 'center',
  },
  root: {
    flexGrow: 1,
    // marginTop: 10, // for `static` header
    marginTop: 70, // for `fixed` header
    padding: 8,
  },
  paper: {
    marginBottom: 8,
    padding: 36,
    // textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
  },
  rowPaddingTop: {
    display: 'flex',
    justifyContent: 'center',
    paddingTop: 8,
  },
  nonCenterRow: {
    display: 'flex',
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 120,
    height: 120,
  },
  avatar2: {
    margin: 5,
  },
  bigAvatar2: {
    width: 30,
    height: 30,
  },
  leftAlignRow: {
    display: 'flex',
    textAlign: 'left',
  },
  rightAlignRow: {
    display: 'flex',
    textAlign: 'right',
  },
  leftAlignRowPaddingTop: {
    display: 'flex',
    textAlign: 'left',
    paddingTop: 16,
  },
  dividerMarginBottom: {
    marginBottom: 20,
  },
  paddingLeft: {
    display: 'flex',
    paddingLeft: 4,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 1,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 8px',
    transform: 'scale(1.5)',
    color: '#FF4081',
  },
  blackBullet: {
    display: 'inline-block',
    margin: '0 8px',
    transform: 'scale(1.5)',
    color: '#212121',
  },
  blankSpace: {
    display: 'inline-block',
    margin: '0 1px',
  },
  tableOfContents: {
    // margin: theme.spacing.unit * 3,
    // borderLeft: '4px solid #3F51B5',
    borderLeft: '4px solid #FF4081',
    paddingLeft: 12,
    // padding: 8,
  },
  hashLink: {
    color: '#1da330',
    '&:link': {
      color: '#1da330',
      textDecoration: 'none',
    },
    '&:visited': {
      textDecoration: 'none',
      color: '#1da330',
    },
    '&:hover': {
      textDecoration: 'none',
      color: '#1da330',
    },
    '&:active': {
      textDecoration: 'none',
      // textDecoration: 'underline',
      color: '#1da330',
    },
  },
  stickyLeftSideBar: {
    // position: '-webkit-sticky',
    position: 'sticky',
    top: 0,
  },
});
