/**
 *
 * Hkcm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { compose } from 'redux';
import { injectIntl } from 'react-intl';
import Typography from 'material-ui/Typography';
import LocationOn from 'material-ui-icons/LocationOn';
import AccessTime from 'material-ui-icons/AccessTime';
import { ColorAWithUnderline } from '../../../components/A/index';
import { styles } from '../styles';
import messages from '../messages';


export class Hkcm extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { classes } = this.props;
    const bull = <span className={classes.bullet}>•</span>;
    // eslint-disable-next-line react/prop-types
    const { formatMessage } = this.props.intl;
    // const blackBull = <span className={classes.blackBullet}>•</span>;
    // const bs = <span className={classes.blankSpace}>{}</span>;
    return (
      <div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            <strong>{formatMessage(messages.hkcmDev)}</strong>
          </Typography>
        </div>
        <Typography type="caption" gutterBottom>
          <AccessTime />
          &nbsp;
          {formatMessage(messages.hkcmDevPeriod)}
        </Typography>
        <Typography type="caption" gutterBottom>
          <LocationOn />
          &nbsp;
          {formatMessage(messages.hkstp)}
        </Typography>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.projName)}&nbsp;:
            &nbsp;
            {formatMessage(messages.projNamehkcm)}
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.projType)}&nbsp;:
            &nbsp;
            {formatMessage(messages.projType2)}
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.projLink)}&nbsp;:
            &nbsp;
            <ColorAWithUnderline href="http://hongkongcrimemap.com" target="_blank">
              http://hongkongcrimemap.com
            </ColorAWithUnderline>
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.projStack)}&nbsp;:
            &nbsp;
            {formatMessage(messages.projStack1)}
          </Typography>
        </div>
        <div className={classes.leftAlignRow}>
          <Typography type="body1" gutterBottom>
            {bull}{formatMessage(messages.projResponsibilities)}&nbsp;:
            &nbsp;
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            {formatMessage(messages.proj1Res1)}
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            {formatMessage(messages.proj1Res2)}
            <br />
          </Typography>
        </div>
        <div className={classes.indentRow}>
          <Typography component="p" gutterBottom>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.&nbsp;
            <br />
          </Typography>
          <Typography component="p" gutterBottom>
            {formatMessage(messages.proj1Res3)}
            <br />
          </Typography>
        </div>
      </div>
    );
  }
}

Hkcm.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default compose(
  injectIntl,
  withStyles(styles),
)(Hkcm);
