/*
 * Resume Messages
 *
 * This contains all the text for the Resume component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  resumeTitle: {
    id: 'app.containers.resume.title',
    defineMessages: 'Résumé - Pharrell.zx WANG',
  },
  pharrellName: {
    id: 'app.containers.resume.pharrellName',
    defineMessages: 'Pharrell.zx WANG',
  },
  email: {
    id: 'app.containers.resume.email',
    defineMessages: 'Email',
  },
  HongKong: {
    id: 'app.containers.resume.HongKong',
    defineMessages: 'Hong Kong',
  },
  Mainland: {
    id: 'app.containers.resume.Mainland',
    defineMessages: 'Mainland',
  },
  degreeOnHold: {
    id: 'app.containers.resume.degreeOnHold',
    defineMessages: 'M.Sc, B.Sc',
  },
  toc: {
    id: 'app.containers.resume.toc',
    defineMessages: 'Table of Contents',
  },
  interests: {
    id: 'app.containers.resume.interests',
    defineMessages: 'Interests',
  },
  academiaExperience: {
    id: 'app.containers.resume.academiaExperience',
    defineMessages: 'Academia Experience',
  },
  workExperience: {
    id: 'app.containers.resume.workExperience',
    defineMessages: 'Work Experience',
  },
  education: {
    id: 'app.containers.resume.education',
    defineMessages: 'Education',
  },
  DeepLearning: {
    id: 'app.containers.resume.DeepLearning',
    defineMessages: 'Deep Learning, Computer Vision, Computational Linguistics',
  },
  fullStackWebAppDev: {
    id: 'app.containers.resume.fullStackWebAppDev',
    defineMessages: 'Full-Stack Web App Development',
  },
  VideoCodingOptimizations: {
    id: 'app.containers.resume.VideoCodingOptimizations',
    defineMessages: 'Video Coding Optimizations',
  },
  DataAnalyticsVisu: {
    id: 'app.containers.resume.DataAnalyticsVisu',
    defineMessages: 'Data Analytics, Data Visualization',
  },
  MScUniv: {
    id: 'app.containers.resume.MScUniv',
    defineMessages: 'The Hong Kong Polytechnic University',
  },
  MScPeriod: {
    id: 'app.containers.resume.MScPeriod',
    defineMessages: 'Sep, 2015 - Dec, 2017',
  },
  BScPeriod: {
    id: 'app.containers.resume.BScPeriod',
    defineMessages: 'Sep, 2011 - July, 2015',
  },
  Award: {
    id: 'app.containers.resume.Award',
    defineMessages: 'Award',
  },
  Major: {
    id: 'app.containers.resume.Major',
    defineMessages: 'Major',
  },
  MScAward: {
    id: 'app.containers.resume.MScAward',
    defineMessages: 'Master of Science',
  },
  MScMajor: {
    id: 'app.containers.resume.MScMajor',
    defineMessages: 'Electronic and Information Engineering',
  },
  MScThesis: {
    id: 'app.containers.resume.MScThesis',
    defineMessages: 'Thesis',
  },
  MScThesisTitle: {
    id: 'app.containers.resume.MScThesisTitle',
    defineMessages: 'Fast Depth Coding in 3D-HEVC Using Deep Learning',
  },
  BScUniv: {
    id: 'app.containers.resume.BScUniv',
    defineMessages: 'Nanjing University of Aeronautics and Astronautics',
  },
  BScAward: {
    id: 'app.containers.resume.BScAward',
    defineMessages: 'Bachelor of Science',
  },
  BScMajor: {
    id: 'app.containers.resume.BScMajor',
    defineMessages: 'Electrical Engineering and Automation',
  },
  MScThesisPeriod: {
    id: 'app.containers.resume.MScThesisPeriod',
    defineMessages: 'May, 2016 - Dec, 2017',
  },
  MScGroup: {
    id: 'app.containers.resume.MScGroup',
    defineMessages: 'Group',
  },
  MScGroupName: {
    id: 'app.containers.resume.MScGroupName',
    defineMessages: 'Centre for Signal Processing',
  },
  MScSupervisor: {
    id: 'app.containers.resume.MScSupervisor',
    defineMessages: 'Supervisor',
  },
  MScSupervisorName: {
    id: 'app.containers.resume.MScSupervisorName',
    defineMessages: 'Dr. Y. L. Chan',
  },
  LatexMScThesis: {
    id: 'app.containers.resume.LatexMScThesis',
    defineMessages: 'Source code for thesis writing',
  },
  MScThesisDocument: {
    id: 'app.containers.resume.MScThesisDocument',
    defineMessages: 'Document',
  },
  MScThesisDocumentTitle: {
    id: 'app.containers.resume.MScThesisDocumentTitle',
    defineMessages: 'Fast Depth Coding Using Deep Learning',
  },
  MScThesisCodebases: {
    id: 'app.containers.resume.MScThesisCodebases',
    defineMessages: 'Codebases',
  },
  MScThesisProj1: {
    id: 'app.containers.resume.MScThesisProj1',
    defineMessages: 'Collect data in Gigabytes scale by encoding sequences',
  },
  MScThesisProj2: {
    id: 'app.containers.resume.MScThesisProj2',
    defineMessages: 'Pre-process the data using Numpy and Pandas',
  },
  MScThesisProj3: {
    id: 'app.containers.resume.MScThesisProj3',
    defineMessages: 'Train the model from scratch in Tensorflow and freeze the computational graph with learned parameters',
  },
  MScThesisProj4: {
    id: 'app.containers.resume.MScThesisProj4',
    defineMessages: 'Integrate the learned models into 3D-HEVC reference software to perform predictions',
  },
  MScThesisContributions: {
    id: 'app.containers.resume.MScThesisContributions',
    defineMessages: 'Contributions',
  },
  MScThesisContribution1: {
    id: 'app.containers.resume.MScThesisContribution1',
    defineMessages: 'A deep neural network with 32 layers comprising ResNet units has been designed and trained to recognize the most probable angular mode of coding units (CUs) in intra-picture prediction in 3D-HEVC encoder. Each learned model has high top-15 precision which works well on tasks of recognizing intra angular patterns in 3D-HEVC.',
  },
  MScThesisContribution2: {
    id: 'app.containers.resume.MScThesisContribution2',
    defineMessages: 'A way of integrating the learned model into HTM16.2 encoder has been suggested. By making use of Bazel to compile the encoder binary, the data level parallelism (instead of concurrency) functionality in CPU as well as the parallel architecture in GPU are fully utilized for efficient matrix computations.',
  },
  MScThesisContribution3: {
    id: 'app.containers.resume.MScThesisContribution3',
    defineMessages: 'An algorithm for fast depth map coding has been proposed and implemented. The simulation results show that the proposed algorithm is capable of reducing 64.6% of encoding time in wedgelet searching during 3D-HEVC encoding process while the BD performance only has a trivial decrease.',
  },
  hkcmDev: {
    id: 'app.containers.resume.hkcmDev',
    defineMessages: 'Hong Kong Crime Map development',
  },
  hkcmDevPeriod: {
    id: 'app.containers.resume.hkcmDevPeriod',
    defineMessages: 'Sep, 2016 - Nov, 2016',
  },
  hkstp: {
    id: 'app.containers.resume.hkstp',
    defineMessages: 'Hong Kong Science Park',
  },
  projName: {
    id: 'app.containers.resume.projName',
    defineMessages: 'Name',
  },
  projType: {
    id: 'app.containers.resume.projType',
    defineMessages: 'Type',
  },
  projType1: {
    id: 'app.containers.resume.projType1',
    defineMessages: 'Full-stack web app development',
  },
  projType2: {
    id: 'app.containers.resume.projType2',
    defineMessages: 'Full-stack web app development + Natural Language Processing',
  },
  projNamehkcm: {
    id: 'app.containers.resume.projNamehkcm',
    defineMessages: 'Hong Kong Crime Map',
  },
  projLink: {
    id: 'app.containers.resume.projLink',
    defineMessages: 'Link',
  },
  projStack: {
    id: 'app.containers.resume.projStack',
    defineMessages: 'Stack',
  },
  projStack1: {
    id: 'app.containers.resume.projStack1',
    defineMessages: 'Stanford CoreNLP, Apache OpenNLP, FudanNLP, Django, DjangoRestFramework, PostgreSQL, ECharts, BeautifulSoup, ... etc.',
  },
  projStack2: {
    id: 'app.containers.resume.projStack2',
    defineMessages: 'React, Redux, Redux Form, Redux Saga, ImmutableJS, Django, DjangoRestFramework, MySQL(v1), PostgreSQL(v2), ECharts, (GraphQL, Relay, MongoDB), ... etc.',
  },
  projStack3: {
    id: 'app.containers.resume.projStack3',
    defineMessages: 'React, Redux, Redux Form, Redux Saga, ImmutableJS, Django, DjangoRestFramework, MySQL, ... etc.',
  },
  projResponsibilities: {
    id: 'app.containers.resume.projResponsibilities',
    defineMessages: 'Responsibilities',
  },
  proj1Res1: {
    id: 'app.containers.resume.proj1Res1',
    defineMessages: 'Develop crawlers to craw data from news websites, use NLP libraries to do tokenization and store the tokenized data into database. Schedule the crawlers to craw data every hour.',
  },
  proj1Res2: {
    id: 'app.containers.resume.proj1Res2',
    defineMessages: 'Design database structure to manage the data, develop backend REST APIs to be consumed by user interface.',
  },
  proj1Res3: {
    id: 'app.containers.resume.proj1Res3',
    defineMessages: 'Visualize the data in the user interface using ECharts.',
  },
  proj2Res1: {
    id: 'app.containers.resume.proj2Res1',
    defineMessages: 'Develop the frontend user interface of the platform with a focus on user experience.',
  },
  proj2Res2: {
    id: 'app.containers.resume.proj2Res2',
    defineMessages: 'Design database structure to manage the data, develop backend REST APIs to be consumed by user interface.',
  },
  proj2Res3: {
    id: 'app.containers.resume.proj2Res3',
    defineMessages: 'Maintain platform version 1.',
  },
  proj2Res4: {
    id: 'app.containers.resume.proj2Res4',
    defineMessages: 'Perform data analytics for Key Performance Indicator(KPI) report monthly.',
  },
  zwapDev: {
    id: 'app.containers.resume.zwapDev',
    defineMessages: 'Fintech platform development',
  },
  zwapDevPeriod: {
    id: 'app.containers.resume.zwapDevPeriod',
    defineMessages: 'Sep, 2016 - Present',
  },
  projNameZwap: {
    id: 'app.containers.resume.projNameZwap',
    defineMessages: 'Fintech P2P Web App',
  },
  zwapPayDev: {
    id: 'app.containers.resume.zwapPayDev',
    defineMessages: 'Zwap Pay development',
  },
  zwapPayDevPeriod: {
    id: 'app.containers.resume.zwapPayDevPeriod',
    defineMessages: 'Nov, 2017 - Dec, 2017',
  },
  projNameZwapPay: {
    id: 'app.containers.resume.projNameZwapPay',
    defineMessages: 'Zwap Pay',
  },
  proj3Res1: {
    id: 'app.containers.resume.proj3Res1',
    defineMessages: 'Provide technical solution for platform collaboration with third-party e-commerce websites.',
  },
  proj3Res2: {
    id: 'app.containers.resume.proj3Res2',
    defineMessages: 'Design database structure to manage the data, develop backend REST APIs to be consumed by user interface.',
  },
  proj3Res3: {
    id: 'app.containers.resume.proj3Res3',
    defineMessages: 'Develop frontend user interface with a focus on the user experience.',
  },
  myGitHub: {
    id: 'app.containers.resume.myGitHub',
    defineMessages: 'My GitHub',
  },
  msgFromAuthor: {
    id: 'app.containers.resume.msgFromAuthor',
    defineMessages: 'Thank you for visiting my page.',
  },
  lastUpdated: {
    id: 'app.containers.resume.lastUpdated',
    defineMessages: 'Last updated: 16 March, 2018',
  },
  appointedAs: {
    id: 'app.containers.resume.appointedAs',
    defineMessages: 'Appointed as',
  },
  jobTitle: {
    id: 'app.containers.resume.jobTitle',
    defineMessages: 'Senior Software Developer',
  },
});
