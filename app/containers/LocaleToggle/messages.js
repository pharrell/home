/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  en: {
    id: 'boilerplate.containers.LocaleToggle.en',
    defaultMessage: 'English 英文',
  },
  zh: {
    id: 'boilerplate.containers.LocaleToggle.zh',
    defaultMessage: 'Chinese 中文',
  },
});
