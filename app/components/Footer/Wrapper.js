import styled from 'styled-components';

const Wrapper = styled.footer`
  display: flex;
  justify-content: space-between;
  padding: 1em 0;
  padding-left: 1em;
  // padding-top: 0em;
  margin-top: 1em;
  // text-align: center;
  // margin-left: 5em;
  border-top: 1px solid #666;
`;

export default Wrapper;
