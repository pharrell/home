import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withStyles } from 'material-ui/styles/index';
import Typography from 'material-ui/Typography';

// import A from 'components/A';
// import LocaleToggle from 'containers/LocaleToggle';
import Wrapper from './Wrapper';
import messages from './messages';

const styles = {
  section: {
    paddingLeft: 10,
  },
};

class Footer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { classes } = this.props;
    return (
      <Wrapper>
        <section className={classes.section}>
          <Typography gutterBottom noWrap>
            &copy; <FormattedMessage {...messages.copyright} /> 2018, <FormattedMessage {...messages.myName} />
          </Typography>
        </section>
      </Wrapper>
    );
  }
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
