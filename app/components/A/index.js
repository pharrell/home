/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';

const A = styled.a`
  color: #616161;
 
  &:link {
    text-decoration: none;
  }
  &:visited {
    text-decoration: none;
  }
  &:hover {
    text-decoration: underline;
  }
  &:active {
    text-decoration: underline;
  }
`;

export const ColorA = styled.a`
  color: #1565c0;
 
  &:link {
    text-decoration: none;
  }
  &:visited {
    text-decoration: none;
  }
  &:hover {
    text-decoration: underline;
  }
  &:active {
    text-decoration: underline;
  }
`;

export const ColorAWithUnderline = styled.a`
  color: #4caf50;
 
  &:link {
    text-decoration: none;
  }
  &:visited {
    text-decoration: none;
    color: #087f23;
  }
  &:hover {
    text-decoration: underline;
    color: #087f23;
  }
  &:active {
    text-decoration: underline;
    color: #087f23;
  }
`;

export default A;
