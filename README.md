# Homepage project 
Intended for personal homepage. Currently only résumé.

## Dev

``yarn && yarn start``

## Deploy to gh-pages

``yarn run deploy``

## Note
1. For security reasons, ``.htaccess.bin`` is not allowed in gh-pages.
    Hence, after you have deployed it to gh-pages, in the console you 
    can see error like: ``Uncaught (in promise) Error: Wrong response status``.
    I think it has no harm. 
    Since in gh-pages, there's no need to touch apache configurations.
2. If you see console complaining about ``sw.js`` or 
    ``the script has an unsupported MIME type ('text/html')``: 
    Open **Chrome Dev Tools** -> 
    **Application** -> **Service Worker** 
    and see if you have *a service worker* has been registered? 
    If yes click unregister and refresh the page again.
    
    Ref: [The script has an unsupported MIME type ('text/html')](https://github.com/facebookincubator/create-react-app/issues/658)

3. checkout ``App/index.js`` to see how [react-ga](https://github.com/react-ga/react-ga) has been added. 

## Hash link scroll with offset

For *react-router-hash-link* to be able to scroll with offsets when the header is sticky:

After ``yarn``, in ``/lib/index.js``, 

right after ``element.scrollIntoView();`` add:

```javascript
var scrolledY = window.scrollY;

if(scrolledY){
  window.scroll(0, scrolledY - '[your header height in pixels]');
  //E.g.,  
  //window.scroll(0, scrolledY - 72);
}
```

**Note**:
>Something strange but acceptable: The above approach for scrolling with offset is not 
effective in when developing the app using ``yarn start``; But it is effective when 
running the app using ``yarn start:production``.
